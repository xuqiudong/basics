package pers.vic.basics.sort;

/**
 * @description:冒泡排序
 * @author Vic.xu
 * @date: 2020/10/15 0015 14:01
 */
public class BubbleSort {
    /*
    比较规则，copy自百度<br/>
    1. 比较相邻的元素。如果第一个比第二个大，就交换他们两个。 <br/>
    2. 对每一对相邻元素做同样的工作，从开始第一对到结尾的最后一对。在这一点，最后的元素应该会是最大的数。 <br/>
    3.  针对所有的元素重复以上的步骤，除了最后一个。 <br/>
    4. 持续每次对越来越少的元素重复上面的步骤，直到没有任何一对数字需要比较。<br/>
     */

    public static void sort(int[] arr){
        int len = arr.length;
        //外层循环表示比较多少轮
        for (int i=1; i<len; i++) {
            //如果一轮比较下来 全部都没有交换数字，表示整个数组都是有序的，也就不需要继续比较了
            boolean orderly = true;
            //内层循环表示比较哪些下标的数据
            for (int j = 0; j < len -i; j++) {
                //前一个元素比后一个元素大 则交换
                if(arr[j] > arr[j+1]) {
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                    orderly = false;
                }
            }
            //有序的，则终止外层循环
            if(orderly) {
                break;
            }
        }

    }
}
