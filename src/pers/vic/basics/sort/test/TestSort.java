package pers.vic.basics.sort.test;

import pers.vic.basics.sort.*;

import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.Consumer;

/**
 * @author Vic.xu
 * @description:测试排序
 * @date: 2020/10/15 14:15
 */
public class TestSort {

    public static void main(String[] args) {
        sort(BubbleSort::sort, "冒泡排序");
        sort(SelectionSort::sort, "选择排序");
        sort(InsertionSort::sort, "插入排序");
        sort(ShellSort::sort, "希尔排序");
        sort(QuickSort::sort, "快速排序");
    }

    /**
     * 测试某个排序功能
     * @param consumer
     * @param sortName
     */
    public static void sort(Consumer<int[]> consumer, String sortName) {
        int[] arr = arraysGenerator(10);
        display(arr, "原始数组");
        consumer.accept(arr);
        display(arr, sortName);
        System.out.println("------------------------------------------------------");
    }

    /**
     * 随机一个数组
     */
    public static int[] arraysGenerator(int size) {
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = ThreadLocalRandom.current().nextInt(30);
        }
        return arr;
    }

    public static void display(int[] arr, String sortName) {
        System.out.print(sortName + ": ");
        System.out.print("\t" + Arrays.toString(arr) + "\n");
    }

}
