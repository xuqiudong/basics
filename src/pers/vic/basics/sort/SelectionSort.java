package pers.vic.basics.sort;

import org.omg.Messaging.SyncScopeHelper;

/**
 * @description:选择排序
 * @author Vic.xu
 * @date: 2020/10/15 0015 14:30
 */
public class SelectionSort {
    /*
    排序原理：
    1. 在未排序序列中找到最小（大）元素，存放到排序序列的起始位置，
    2. 从剩余未排序元素中继续寻找最小（大）元素，然后放到已排序序列的末尾。
    3. 以此类推，直到所有元素均排序完毕
     */

    public  static void sort(int[] arr){
        int len =arr.length;
        //外部循环 表示每一次找出的最小值需要放置的位置
        for (int i = 0; i < len-1; i++) {
            int minIndex = i;
            //内层循环 找出为排序的数值中的最小值
            for (int j = i+1; j < len; j++) {
                if(arr[minIndex] > arr[j]){
                    minIndex = j;
                }
            }
            //交换位置
            if(minIndex != i) {
                int temp = arr[i];
                arr[i] = arr[minIndex];
                arr[minIndex] = temp;
            }
        }
    }
}
