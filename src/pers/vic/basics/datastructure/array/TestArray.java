package pers.vic.basics.datastructure.array;

/**
 * @author Vic.xu
 * @description:
 * @date: 2020/10/15  11:30
 */
public class TestArray {

    public static void main(String[] args) {
        VicArray array = new VicArray(5);

        array.add(1);
        array.add(2);
        array.add(3);
        array.add(4);
        System.out.println("size : " + array.size());
        array.display();

        array.add(5);
        array.add(6);
        array.display();

        System.out.println("index[4] = " + array.get(4));
        System.out.println("修改[5] 为 [50]");
        array.modify(5,50);
        array.modify(55,50);
        array.display();
        System.out.println("size : " + array.size());
        array.delete(3);
        array.display();
        // 下标越界
        System.out.println(array.get(5));

    }
}
