package pers.vic.basics.datastructure.array;

/**
 * @author Vic.xu
 * @description:数组接口
 * @date: 2020/10/15 0015 10:11
 */
public interface ArrayInterface<T> {
  /**
 *
 * 1、 插入数据
 * 2. 查找数据
 * 3. 删除数据
 * 4. 访问数据
 * 5.修改数据
 * 6. 展示数据
 */
  /**插入数据*/
  boolean add(T t);

  /**查找数据*/
  int search(T t);

  /**删除数据*/
  boolean delete(T t);

  /**获取数据*/
  T get(int index);

  /**修改数据*/
  boolean modify(T old, T t);

  /**展示数据*/
  void display();

  /**数组长度*/
  int size();

}
