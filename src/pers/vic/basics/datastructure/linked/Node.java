package pers.vic.basics.datastructure.linked;

/**
 * @author Vic.xu
 * @description: 单向 双向链表节点
 * @date: 2020/10/19 0019 16:41
 */
public class Node {

    Object data;

    Node next;

    public Node(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Node{" +
                "data=" + data +
                ", next=" + next +
                '}';
    }
}
