package pers.vic.basics.datastructure.linked;

/**
 * @description:链表:
 * 单向链表、双端链表、有序链表、双向链表以及有迭代器的链
 * @author Vic.xu
 * @date: 2020/10/16 0016 16:17
 */
public interface LinkedInterface {

    /**
     * 新增头节点
     */
    Object addHead(Object obj);

    /**
     * 尾部追加节点
     * @param obj
     * @return
     */
    Object addTail(Object obj);

    /**
     * 删除头节点
     */
    Object deleteHead();

    /**
     * 查找节点
     */
    Node find(Object obj);

    /**删除指定的节点*/
    boolean delete(Object obj);

    /**
     * 节点是否为空
     */
    boolean isEmpty();

    /**
     * 打印链表
     */
    void display();

    int size();

}
