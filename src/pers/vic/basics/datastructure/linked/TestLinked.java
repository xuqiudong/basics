package pers.vic.basics.datastructure.linked;

/**
 * @author Vic.xu
 * @description:
 * @date: 2020/10/16 0016 17:07
 */
public class TestLinked {

    public static void main(String[] args) {
        linked(new SingleLinkedList());

        //       linked( new DoublePointLinkedList());

    }

    static int size = 5;

    public static void linked(LinkedInterface linked) {

        for (int i = 0; i < size; i++) {
            linked.addHead(i + 1);
        }
        linked.addTail(666);
        linked.addTail(777);
        linked.display();
        System.out.println(linked.size());
        linked.deleteHead();
        linked.display();
        Node node2 = linked.find(777);

        System.out.println(node2);
        linked.delete(777);
        linked.display();
    }
}
