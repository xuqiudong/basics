/**
 * 基础：数据结构和算法
 * <ul>
 *     <li>arrar包：数组</li>
 *     <li>linked包：链表</li>
 *     <li>stack包：栈</li>
 *     <li>tree包：树</li>
 *
 * </ul>
 */
package pers.vic.basics.datastructure;