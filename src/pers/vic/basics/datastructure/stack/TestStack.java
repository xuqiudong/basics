package pers.vic.basics.datastructure.stack;

/**
 * @author Vic.xu
 * @description:
 * @date: 2020/10/15 0015 17:06
 */
public class TestStack<基本测试> {

    public static void main(String[] args) {
//        test();
        reversal("hello boys");
        String string = "[aasd{asd<asd>}]";
        match(string);
        testStakeByLinked();
    }

    /**通过栈反转字符串*/
    public static void reversal(String string){
        VicStack<Character> stack = new VicStack<>();
        char[] cs = string.toCharArray();
        for (char c : cs) {
            stack.push(c);
        }
        while (!stack.isEmpty()) {
            System.out.print(stack.pop());
        }
        System.out.println();
    }



    /**
     * 通过栈判断 < [ { 是否成对匹配
     * 思路：
     * 遇到< [ { 就往栈中push
     * 遇到> [ { 就从栈中取出 看看是否匹配
     */

    public static void match(String string){
        VicStack<Character> stack = new VicStack<>();
        char[] cs = string.toCharArray();
        for (char c : cs) {
           switch (c) {
               case '{':
               case '[':
               case '<':
                   stack.push(c);
                   break;
               case '}':
               case ']':
               case '>':


                   if(stack.isEmpty()) {
                       System.out.println(c + " 不匹配");
                       return;
                   }
                   Character c1 = stack.pop();
                   boolean matched = (c1 == '{' && c == '}') || (c1 == '[' && c == ']') || (c1 == '<' && c == '>');
                   if(!matched){
                       System.out.println(c + " 不匹配");
                       return;
                  }

                   break;
               default:
                   break ;
           }
        }
        System.out.println("字符串[" + string+ "]是符合规范的");
    }

    /**
     * 测试通过链表实现的栈
     */
    public static void testStakeByLinked(){
        String string = "我是谁";
        StackByLinked stack = new StackByLinked();
        char[] cs = string.toCharArray();
        for (char c : cs) {
            stack.push(c);
        }
        while (!stack.isEmpty()) {
            System.out.print(stack.pop());
        }
        System.out.println();
    }

    /**
     * 基本测试
     */
    public static void test() {
        VicStack<Integer> stack = new VicStack<>(4);
        stack.display();
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        stack.display();
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        stack.display();
        System.out.println(stack.peek());
        stack.display();
    }

}
