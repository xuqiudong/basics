package pers.vic.basics.datastructure.stack;

/**
 * @description: 栈
 * @author Vic.xu
 * @date: 2020/10/15 0015 16:25
 */
public interface StackInterface<T> {

    /*
    栈：
    只能在一端进行插入和删除操作的特殊线性表 <br />
    先进后出
    先进入的数据被压入栈底，最后的数据在栈顶，
    从栈顶开始弹出数据
     */

    /**出栈：弹出数据*/
    T pop();

    /**进栈：插入数据*/
    boolean  push(T element);

    /**访问栈顶数据*/
    T  peek();

    /**栈空了否*/
    boolean isEmpty();

    /**栈满了否*/
    boolean isFull();
}
