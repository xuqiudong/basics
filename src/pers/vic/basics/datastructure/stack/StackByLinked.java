package pers.vic.basics.datastructure.stack;

import pers.vic.basics.datastructure.linked.SingleLinkedList;

/**
 * @description:通过链表实现栈的功能
 * @author Vic.xu
 * @date: 2020/10/16 17:17
 */
public class StackByLinked implements StackInterface<Object>{

    SingleLinkedList datas;

    public StackByLinked(){
        this.datas = new SingleLinkedList();
    }

    /**
     * 弹出数据
     */
    @Override
    public Object pop() {
        return datas.deleteHead();
    }

    @Override
    public boolean push(Object element) {
        datas.addHead(element);
        return true;
    }

    @Override
    public Object peek() {
        return datas.getHeadData();
    }

    @Override
    public boolean isEmpty() {
        return datas.isEmpty();
    }

    @Override
    public boolean isFull() {
        return false;
    }
}
