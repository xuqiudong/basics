package pers.vic.basics.datastructure.tree.test;

import pers.vic.basics.datastructure.tree.BinaryNode;
import pers.vic.basics.datastructure.tree.BinarySearchTree;
import pers.vic.basics.datastructure.tree.TreePrinter;

/**
 * @author Vic.xu
 * @description:
 * @date: 2020/10/27 0027 10:21
 */
public class TestTree {

    public static void main(String[] args) {
        binaryTreeInsert();
    }



    public static void binaryTreeInsert(){
        // 通过插入顺序 构建个平衡树 试试
        BinarySearchTree tree = new BinarySearchTree();
        tree.insert("08");

        tree.insert("04");
        tree.insert("12");

        tree.insert("02");
        tree.insert("06");
        tree.insert("10");
        tree.insert("14");

        tree.insert("01");
        tree.insert("03");
        tree.insert("05");
//        tree.insert("07");
        tree.insert("09");
        tree.insert("11");
        tree.insert("13");
        tree.insert("15");
        TreePrinter.show(tree.getRoot());
        System.out.print("中序遍历");
        tree.infixOrder();
        System.out.println();
        System.out.print("前序遍历");
        tree.preOrder();
        System.out.println();
        System.out.print("后序遍历");
        tree.postOrder();
        System.out.println();

        Object key = "04";
        BinaryNode node = tree.find(key);
        if (node != null) {
            node.display();
        }else {
            System.out.println("没有找到:" + key);
        }
        System.out.println("max: " + tree.max());
        System.out.println("min: " + tree.min());
        Object del = "06";
        System.out.println("删除1度节点" + del);
        tree.delete(del);
        TreePrinter.show(tree.getRoot());

        Object del2 = "01";
        System.out.println("删除0度节点" + del2);
        tree.delete(del2);
        TreePrinter.show(tree.getRoot());

        Object del3 = "12";
        System.out.println("删除2度节点" + del3);
        tree.delete(del3);
        TreePrinter.show(tree.getRoot());

    }
}
