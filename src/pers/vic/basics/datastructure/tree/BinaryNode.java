package pers.vic.basics.datastructure.tree;

/**
 * @author Vic.xu
 * @description: 二叉树的节点
 * @date: 2020/10/26 0026 14:55
 */
public class BinaryNode {

    protected Object data;

    protected BinaryNode parent;

    protected BinaryNode left;

    protected BinaryNode right;

    public void display() {
        Object l = left == null ? "〇" : left.getData();
        Object r = right == null ? "〇" : right.getData();
        System.out.println(data + "[" + l + " - " + r + "]");
    }

    @Override
    public String toString() {
        display();
        return data + "";
    }

    public BinaryNode(Object data) {
        this.data = data;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public BinaryNode getLeft() {
        return left;
    }

    public void setLeft(BinaryNode left) {
        this.left = left;
    }

    public BinaryNode getRight() {
        return right;
    }

    public void setRight(BinaryNode right) {
        this.right = right;
    }

    public BinaryNode getParent() {
        return parent;
    }

    public void setParent(BinaryNode parent) {
        this.parent = parent;
    }
}
