package pers.vic.basics.algorithm.dfs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @description: 77. 组合 {@literal https://leetcode-cn.com/problems/combinations/}
 * @author Vic.xu
 * @date: 2020/11/30 0030 15:33
 */
public class Leetcode0077_M_combine {
    /**
     * 给定两个整数 n 和 k，返回 1 ... n 中所有可能的 k 个数的组合。
     */

    public static List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> result = new ArrayList<>();
        List<Integer> path = new ArrayList<>();
        dfs(result, path, n, k, 1);
        return result;
    }

    public static void dfs(List<List<Integer>> result, List<Integer> path, int n, int k, int start) {

        if (path.size() == k) {
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = start; i <= n; i++) {
            path.add(i);
            dfs(result, path, n, k, i + 1);
            path.remove(path.size() - 1);

        }
    }


    public static void main(String[] args) {
        combine(3, 2).forEach(System.out::println);
    }
}
