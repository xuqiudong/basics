/**
 * 这个包是用来总结有一些算法的
 * <p>
 *     比如：
 *     递归，
 *     贪心，greedy
 *     回溯  dfs，
 *     枚举，
 *     动态规划    dp
 *     单调栈： monotone
 *     二分法详解 @see <a href = "https://leetcode-cn.com/problems/search-in-rotated-sorted-array-ii/solution/yi-wen-dai-ni-gao-ding-er-fen-sou-suo-ji-ki52/">二分法详解</a>
 *
 * </p>
 */
package pers.vic.basics.algorithm;