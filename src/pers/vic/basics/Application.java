package pers.vic.basics;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Properties;
import java.util.Stack;
import java.util.concurrent.TimeUnit;

/**
 * @author Vic.xu
 * @description: Maybe some tests
 * @date: 2020/11/9  9:47
 */
public class Application {
    public static void main(String[] args) throws IOException {
        String a = "D:/temp/case/a.txt";
        System.out.println(new File(a).exists());

        /*

         */
//        modifyJvmPropertiesDynamicTest();

    }

    public Request test2(){
        Request r = new Request(12);
        change(r);
        return r;
    }

    public void change(Request r){
        r = new Request(11);
    }

    class Request{

        int id ;

        public Request(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }

    public static void test() {
        //一个整数，它加上100后是一个完全平方数，再加上168又是一个完全平方数，请问该数是多少？
        /*
        100 + n = a^2
        168 + n = b^2
        68 = b^2 - a^2
         */

        for (int i = 0; i < Integer.MAX_VALUE - 168; i++) {
            if (isAllSqrt(i+100) && isAllSqrt(i+168)) {
                System.out.println(i);
                break;
            }
        }
    }

    public static boolean isAllSqrt(int num) {
        return Math.sqrt(num) % 1 == 0;
    }

    /*
    jinfo
    -flag <name> pid：打印指定JVM的参数值
    -flag [+|-]<name> pid：设置指定JVM参数的布尔值
    -flag <name>=<value> pid：设置指定JVM参数的值
     */
    public static void modifyJvmPropertiesDynamicTest() {
        String key = "mail.test";
        System.setProperty(key, "123456");
        new Thread(() -> {
            while (true) {
                try {
                    Properties properties = System.getProperties();
                    System.out.println(key + " -> " + properties.get(key));
                    TimeUnit.SECONDS.sleep(10);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }

    public static void delete() {
        String source = "D:\\tools\\dev\\_setup-package\\birt-report-framework-4.8.0-20180626";

        String target = "D:\\tools\\dev\\eclipse2020\\eclipse";

        DeleteFile.deleteTargetBySource(source, target);
    }


    public static class DeleteFile {

        static ThreadLocal<Integer> localNumber3 = new ThreadLocal<Integer>() {
            @Override
            protected Integer initialValue() {
                return 0;
            }
        };

        static ThreadLocal<Integer> localNumber = ThreadLocal.withInitial(()-> 0);

        /**
         * 根据源中的文件层级删除目标文件夹中的文件
         *
         * @param source 源文件夹
         * @param target 要删除的目标文件夹
         */
        public static void deleteTargetBySource(String source, String target) {
            try {
                deleteBySource(source, target);
                System.out.println("共删除文件：" + localNumber.get());
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                localNumber.remove();
            }
        }

        /**
         * 根据源中的文件层级删除目标文件夹中的文件
         *
         * @param source 源文件夹
         * @param target 要删除的目标文件夹
         */
        private static void deleteBySource(String source, String target) throws IOException {
            Files.list(Paths.get(source)).map(Path::toFile).forEach(f -> {
                String dname = f.getName();
                if (f.isDirectory()) {

                    try {
                        deleteBySource(source + File.separator + dname, target + File.separator + dname);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    File targerFile = new File(target, dname);
                    if (targerFile.exists()) {
                        System.out.println(targerFile.getAbsoluteFile() + " will be delete....");
                        targerFile.delete();

                        localNumber.set(localNumber.get() + 1);
                    } else {
                        System.out.println(targerFile.getAbsoluteFile() + " not exist");
                    }
                }

            });
        }

    }
}
