package pers.vic.basics.leetcode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Vic.xu
 * @description:0001 两数之和：
 * 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
 * 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。
 * @date: 2020/10/28 0028 14:17
 */
public class J0001_E_TwoSum {

    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for (int i = 0; i < nums.length; i++) {
            int current = nums[i];
            int other = target - current;
            /*
                判断map中是否存在和当前值相加等于目标值的数据
               存在则返回，不存在则放到map中去
            * */
            if (map.containsKey(other)) {
                return new int[]{i, map.get(other)};
            }
            map.put(current, i);
        }
        return null;
    }


    public int[] twoSum2(int[] nums, int target) {
        int len = nums.length;
        for (int i = 0; i < len; i++) {
            for (int j = 0; j < len && i != j; j++) {
                if (nums[i] + nums[j] ==target) {
                    return new int[]{i,j};
                }
            }

        }
        return null;
    }
}
