package pers.vic.basics.leetcode;

import com.sun.org.apache.bcel.internal.generic.RETURN;

/**
 * @description:65. 有效数字   {@literal https://leetcode-cn.com/problems/valid-number/}
 * @author Vic.xu
 * @date: 2020/12/25 0025 10:42
 */
public class J0065_H_IsNumber {
    /*
    验证给定的字符串是否可以解释为十进制数字。
    "0" => true
    " 0.1 " => true
    "abc" => false
    "1 a" => false
    "2e10" => true
    " -90e3   " => true
    " 1e" => false
    "e3" => false
    " 6e-1" => true
    " 99e2.5 " => false
    "53.5e93" => true
    " --6 " => false
    "-+3" => false
    "95a54e53" => false

     */

    /**
     * 这一题我不知道怎么说，按部就班试试

     */
    private static String plus = "+";
    private static String min = "-";
    private static String point = ".";
    private static String natural = "e";

    public static boolean isNumber(String s) {
        //1. 判空 和trim
        if (s == null || (s = s.trim()).isEmpty()) {
            return false;
        }
        s = s.toLowerCase();
        s = s.replace(plus, min);
        //2 判断e
        int eCount = appearCount(s, natural);
        if (eCount > 1) {
            return false;
        }
        //包含e的数字
        if (eCount == 1) {
            String[] ss = s.split(natural);
            if (ss.length != 2) {
                return false;
            }
            return isDigitOrDecimal(ss[0]) && isDigit(ss[1]);
        }

        //不包含e的数
        return isDigitOrDecimal(s);
    }

    //是小数或者整数
    public static boolean isDigitOrDecimal(String s) {
        //判断符号
        if (s.startsWith(min)) {
            s = s.substring(1);
        }
        if (s.contains(min)) {
            return false;
        }
        if (s.isEmpty()) {
            return false;
        }
        int minIndex = s.indexOf(min);
        if (minIndex != -1 && minIndex != 0) {
            return false;
        }
        int count = appearCount(s, point);
        if (count == 0) {
            return isDigit(s);
        }
        if (count > 1) {
            return false;
        }

        //包含一个点时
        if (s.startsWith(point)) {
            s= s.substring(1);
            return isDigit(s);
        }
        if (s.endsWith(point)) {
            s= s.substring(0, s.length()-1);
            return isDigit(s);
        }
        String[] ss = s.split("\\" + point);
        return ss.length == 2 && isDigit(ss[0]) && isDigit(ss[1]);
    }

    //整数
    public static boolean isDigit(String s) {
        //判断符号
        if (s.startsWith(min)) {
            s = s.substring(1);
        }
        if (s.isEmpty()) {
            return false;
        }

        for (char c : s.toCharArray()) {
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }

    //某个字符串先的次数
    private static int appearCount(String str, String sub) {
        String re = str.replace(sub, "");
        return str.length() - re.length();
    }



    public static void main(String[] args) {
        String[] strings = {
                " 005047e+6",//true
                "+.8", // true
                ".",//false
                "-1.",
                ".-4",
                "4e+",
                ".1",
                "3.",
                "0",
                " 0.1 ",
                "abc",
                "1 a",
                "2e10",
                " -90e3   ",
                " 1e",
                "e3",
                " 6e-1",
                " 99e2.5 ",
                "53.5e93",
                " --6 ",
                "-+3",
                "95a54e53"
        };
        /*
         "0" => true
    " 0.1 " => true
    "abc" => false
    "1 a" => false
    "2e10" => true
    " -90e3   " => true
    " 1e" => false
    "e3" => false
    " 6e-1" => true
    " 99e2.5 " => false
    "53.5e93" => true
    " --6 " => false
    "-+3" => false
    "95a54e53" => false
         */
        for (String string : strings) {
            System.out.println(string + " -> " + isNumber(string));
        }

        System.out.println(strings[0].replace(plus, min));
        System.out.println(isNumber("0.1"));
        System.out.println("3.".substring(0,"3.".length()-1));
    }
}
