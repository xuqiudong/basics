package pers.vic.basics.leetcode;

/**
 * @author Vic.xu
 * @description:941. 有效的山脉数组
 * @date: 2020/11/3  13:43
 */
public class J0941_E_MountainArray {
    /*
    给定一个整数数组 A，如果它是有效的山脉数组就返回 true，否则返回 false。

        让我们回顾一下，如果 A 满足下述条件，那么它是一个山脉数组：
        A.length >= 3
        在 0 < i < A.length - 1 条件下，存在 i 使得：
        A[0] < A[1] < ... A[i-1] < A[i]
        A[i] > A[i+1] > ... > A[A.length - 1]

        提示：
        0 <= A.length <= 10000
        0 <= A[i] <= 10000
     */


    public static boolean validMountainArray(int[] A) {
        int len = A.length;
        if (len < 3) {
            return false;
        }
        int i = 0;
        int j = len - 1;
        while (i < len - 1 && A[i] < A[i + 1]) {
            i++;
        }
        while (j > 0 && A[j] < A[j-1]) {
           j--;
        }

        return i != 0 && j != (len - 1) && i == j;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 2, 1};
        System.out.println(validMountainArray(arr));
    }

}
