package pers.vic.basics.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @description: 78. 子集  {@literal https://leetcode-cn.com/problems/subsets/}
 * @author Vic.xu
 * @date: 2021/1/14 0014 8:51
 */
public class J0078_M_Subsets {
    /*
    给你一个整数数组 nums ，返回该数组所有可能的子集（幂集）。解集不能包含重复的子集。
    输入：nums = [1,2,3]
    输出：[[],[1],[2],[1,2],[3],[1,3],[2,3],[1,2,3]]
     */

    /**
     * 示例中的输出如下，感觉有点 动态规划的样子
     * 1. 空元素 []
     * 2. 在前面的每个元素中追加当前元素1 [1]
     * 3. 在前面的每个元素后面追加当前元素2  [2] [1,2]
     * 4. 在前面的每个元素后面追加当前元素3  [3] [2,3] [1,2,3]
     */
    public static List<List<Integer>> subsets(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        //追加第一个空元素
        result.add(new ArrayList<Integer>());
        for (int i = 0; i < nums.length; i++) {
            //当前元素
            int cur = nums[i];
            int preSize = result.size();
            //获取结果集中的各个list ，并在list后追加当前元素
            for (int j = 0; j < preSize; j++) {
                List<Integer> pre = new ArrayList<>(result.get(j));
                pre.add(cur);
                result.add(pre);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        subsets(new int[]{1, 2, 3}).forEach(System.out::println);
    }
}
