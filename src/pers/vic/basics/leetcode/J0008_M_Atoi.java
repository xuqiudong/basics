package pers.vic.basics.leetcode;

/**
 * @author Vic.xu
 * @description:8. 字符串转换整数 (atoi)
 * 说实话 这样的题目 除了  条件细节判断之外  我不知道有什么用意
 * @date: 2020/11/3  11:24
 */
public class J0008_M_Atoi {
    /*
        首先，该函数会根据需要丢弃无用的开头空格字符，直到寻找到第一个非空格的字符为止。接下来的转化规则如下：
        如果第一个非空字符为正或者负号时，则将该符号与之后面尽可能多的连续数字字符组合起来，形成一个有符号整数。
        假如第一个非空字符是数字，则直接将其与之后连续的数字字符组合起来，形成一个整数。
        该字符串在有效的整数部分之后也可能会存在多余的字符，那么这些字符可以被忽略，它们对函数不应该造成影响。
        注意：假如该字符串中的第一个非空格字符不是一个有效整数字符、字符串为空或字符串仅包含空白字符时，则你的函数不需要进行转换，即无法进行有效转换。
        在任何情况下，若函数不能进行有效的转换时，请返回 0 。
        提示：

        本题中的空白字符只包括空格字符 ' ' 。
        假设我们的环境只能存储 32 位大小的有符号整数，那么其数值范围为 [−231,  231 − 1]。如果数值超过这个范围，请返回  INT_MAX (231 − 1) 或 INT_MIN (−231) 。
     */

    /**
     * 处理方式和0007差不多  除了多了几步判断
     *
     * @param s string
     * @return -2147483648 < res < 2147483647
     */
    public static int myAtoi(String s) {
        if (s == null || (s = s.trim()) == "") {
            return 0;
        }
        //是否负数
        boolean negative = false;
        int start = 0;
        if (s.startsWith("-")) {
            start++;
            negative = true;
        } else if (s.startsWith("+")) {
            start++;
        }

        char[] chars = s.toCharArray();
        int len = chars.length;
        int res = 0;
        for (int i = start; i < len; i++) {
            char c = chars[i];
            if (!Character.isDigit(c)) {
                break;
            }
            int num = c - '0';
            //判断是否溢出
            if (!negative && (res > Integer.MAX_VALUE / 10 || (res == Integer.MAX_VALUE / 10 && num > 7))) {
                return Integer.MAX_VALUE;
            }
            if (negative && (res > Integer.MAX_VALUE / 10 || (res == Integer.MAX_VALUE / 10 && num > 8))) {
                return Integer.MIN_VALUE;
            }
            res = res * 10 + num;

        }

        return negative ? res * -1 : res;
    }

    public static void main(String[] args) {
        String s = "   -42";
        int res = myAtoi(s);
        System.out.println(res);
        System.out.println(myAtoi("-91283472332"));
        System.out.println(myAtoi("2147483648"));//需要 2147483647


    }
}
