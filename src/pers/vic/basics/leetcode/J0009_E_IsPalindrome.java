package pers.vic.basics.leetcode;

import java.util.Stack;

/**
 * @author Vic.xu
 * @description: 9. 回文数
 * 判断一个整数是否是回文数。回文数是指正序（从左向右）和倒序（从右向左）读都是一样的整数。
 * 你能不将整数转为字符串来解决这个问题吗？
 * @date: 2020/11/4  8:28
 */
public class J0009_E_IsPalindrome {

    /**
     * 试一试用栈
     */
    public static boolean isPalindrome(int x) {
        if (x < 0) {
            return false;
        }
        if (x < 10) {
            return true;
        }
        Stack<Integer> stack = new Stack<>();
        int temp = x;
        //先入栈 从末尾开始
        while (temp != 0) {
            stack.push(temp % 10);
            temp /= 10;
        }
        int r = 0;
        //当前进位
        int pow = 1;
        //再出栈 我从原高位开始
        while (!stack.isEmpty()) {
            r = stack.pop() * pow + r;
            pow *= 10;
        }
        return r == x;
    }

    public static void main(String[] args) {
        System.out.println(isPalindrome(10));
        System.out.println(isPalindrome(12321));
        System.out.println(isPalindrome(-10));
    }


}

