package pers.vic.basics.leetcode;

/**
 * @description: 38. 外观数列  {@literal https://leetcode-cn.com/problems/count-and-say/}
 * @author Vic.xu
 * @date: 2020/12/2 0002 13:24
 */
public class J0038_E_CountAndSay {
    /*
    给定一个正整数 n ，输出外观数列的第 n 项。
    「外观数列」是一个整数序列，从数字 1 开始，序列中的每一项都是对前一项的描述。
    你可以将其视作是由递归公式定义的数字字符串序列：
    countAndSay(1) = "1"
    countAndSay(n) 是对 countAndSay(n-1) 的描述，然后转换成另一个数字字符串。
    前五项如下：
    1.     1
    2.     11
    3.     21
    4.     1211
    5.     111221
    第一项是数字 1
    描述前一项，这个数是 1 即 “ 一 个 1 ”，记作 "11"
    描述前一项，这个数是 11 即 “ 二 个 1 ” ，记作 "21"
    描述前一项，这个数是 21 即 “ 一 个 2 + 一 个 1 ” ，记作 "1211"
    描述前一项，这个数是 1211 即 “ 一 个 1 + 一 个 2 + 二 个 1 ” ，记作 "111221"
     */
    public static String countAndSay(int n) {
        String count = "1";
        if (n == 1) {
            return count;
        }
        for (int i = 1; i < n; i++) {
            count = say(count);
        }
        return count;
    }

    public static String say(String count){
        char[] cs = count.toCharArray();
        char pre = cs[0];
        int num = 0;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < cs.length; i++) {
            if (cs[i] == pre) {
                num++;
            }else {
                sb.append(num).append(pre);
                pre = cs[i];
                num = 1;
            }
        }
        sb.append(num).append(pre);
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(countAndSay(5));
    }
}
