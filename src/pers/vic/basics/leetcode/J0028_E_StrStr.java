package pers.vic.basics.leetcode;

import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * @description: 28. 实现 strStr()  {@literal https://leetcode-cn.com/problems/implement-strstr/}
 * @author Vic.xu
 * @date: 2020/11/18 8:40
 */
public class J0028_E_StrStr {
    /*
    给定一个 haystack 字符串和一个 needle 字符串，在 haystack 字符串中找出 needle 字符串出现的第一个位置 (从0开始)。
    如果不存在，则返回  -1。
     */

    /**
     * 既然是简单， 那么我  haystack.indexOf(needle) 多简单
     *
     * 解题： 匹配到开头的时候，双指针往下匹配
     *
     *  或者 通过循环haystack  然后substring(i,i+needle.length()) .equals (needle) 这个方法略
     */
    public static int strStr(String haystack, String needle) {
        if (haystack == null || needle == null || haystack.length() < needle.length()) {
            return -1;
        }
        if (needle.length() == 0) {
            return 0;
        }
        char start = needle.charAt(0);
        for (int i = 0; i <= haystack.length() - needle.length(); i++) {
            if (haystack.charAt(i) == start) {
                boolean matched = true;
                int k = i + 1;
                for (int j = 1; j < needle.length(); j++, k++) {
                    if (needle.charAt(j) !=haystack.charAt(k)){
                        matched = false;
                        break;
                    }
                }
                if (matched) {
                    return i;
                }
            }
        }
        return -1;
    }


    public static void main(String[] args) {
        System.out.println(strStr("a", "a"));
        System.out.println(strStr("aaaaa", "bba"));

    }
}
