package pers.vic.basics.leetcode;

import jdk.nashorn.internal.ir.BreakableNode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Vic.xu
 * @description:6. Z 字形变换
 * 将一个给定字符串根据给定的行数，以从上往下、从左到右进行 Z 字形排列。
 * 之后，你的输出需要从左往右逐行读取，产生出一个新的字符串.
 * vic：这不是明明是N型转换吗
 * @date: 2020/11/2 8:47
 */
public class J0006_M_Zconvert {

    /**
     * 我只会暴力解决......
     * a   e   i
     * b d f h j
     * c   g   k
     */
    public static String convert(String s, int numRows) {
        if (numRows == 1) {
            return s;
        }
        List<StringBuilder> sbList = new ArrayList<>();
        for (int i = 0; i < numRows; i++) {
            sbList.add(new StringBuilder());
        }
        int len = s.length();
        //列
        int column = len / numRows;
        int rowIndex = 0;
        //字符下标
        int index = 0;
        //自上往下
        boolean asc = true;
        while (index < len) {
            //自上而下(取numRows-1个)
            for (; rowIndex < numRows - 1 && asc && (index < len); rowIndex++) {
                sbList.get(rowIndex).append(s.charAt(index++));
                //当下一次到达最后一行的时候 标注为下次遍历 是从下往上
                if (rowIndex + 2 == numRows) {
                    asc = !asc;
                }
            }
            //自下而上(取numRows-1个)
            for (; rowIndex > 0 && !asc && (index < len); rowIndex--) {
                sbList.get(rowIndex).append(s.charAt(index++));
                //当下一次达到第一行的时候，标注下次遍历是从上往下
                if (rowIndex == 1) {
                    asc = !asc;
                }
            }
        }

        StringBuilder res = new StringBuilder();
        for (StringBuilder sb : sbList) {
            res.append(sb);
        }

        return res.toString();
    }

    public static void main(String[] args) {
        String s = "abcdefghijk";
        System.out.println(convert(s, 4));
    }
}
