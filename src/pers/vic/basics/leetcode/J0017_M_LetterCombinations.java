package pers.vic.basics.leetcode;

import java.util.ArrayList;
import java.util.List;

/**
 * @description:17. 电话号码的字母组合
 * @author Vic.xu
 * @date: 2020/11/10 15:27
 */
public class J0017_M_LetterCombinations {
    /*
    给定一个仅包含数字 2-9 的字符串，返回所有它能表示的字母组合。
    给出数字到字母的映射如下（与电话按键相同）。注意 1 不对应任何字母
     */

    /**
     * 电话号码的字母组合:
     *  <p>
     *      递归查
     *      各个位置的字符直接罗列即可
     *      边界就是，每个位置都罗列完的时候加入的结果集当中
     *      还有就是当前位后的位置的字符罗列结束的时候，需把临时字符串的位置重置
     *  </p>
     * @param digits
     * @return
     */
    public static List<String> letterCombinations(String digits) {
        List<String> result = new ArrayList<>();
        if(digits == null || digits.isEmpty()){
            return result;
        }


        pick(digits, 0, new StringBuilder(), result);
        return result;
    }

    // 2 -9 对应的字母
    private static String[] map = {"", "", "abc", "edf", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};

    private static void pick(String digits, int index, StringBuilder sb, List<String> result) {
        //当查找到字符串结尾的时候 结束
        if (sb.length() == digits.length()) {
            result.add(sb.toString());
            return;
        }
        //当前数字对应的字符数组
        char[] cs = map[digits.charAt(index) - '0'].toCharArray();
        int sbLen = sb.length();
        for (int i = 0; i < cs.length; i++) {
            sb.append(cs[i]);
            //继续选择下一个字符 直到选择结束
            pick(digits, index + 1, sb, result);
            //继续本次循环之前重置临时字符串的长度
            sb.setLength(sbLen);
        }
    }

    public static void main(String[] args) {
        letterCombinations("223").forEach(System.out::println);
    }
}
