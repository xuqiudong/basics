package pers.vic.basics.leetcode;

import java.sql.SQLOutput;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.Stack;

/**
 * @description: 71. 简化路径 {@literal https://leetcode-cn.com/problems/simplify-path/}
 * @author Vic.xu
 * @date: 2021/1/5 0005 8:28
 */
public class J0071_M_SimplifyPath {
    /*
    以 Unix 风格给出一个文件的绝对路径，你需要简化它。或者换句话说，将其转换为规范路径。
    在 Unix 风格的文件系统中，一个点（.）表示当前目录本身；此外，两个点 （..） 表示将目录切换到上一级（指向父目录）；两者都可以是复杂相对路径的组成部分。更多信息请参阅：Linux / Unix中的绝对路径 vs 相对路径
    请注意，返回的规范路径必须始终以斜杠 / 开头，并且两个目录名之间必须只有一个斜杠 /。最后一个目录名（如果存在）不能以 / 结尾。此外，规范路径必须是表示绝对路径的最短字符串。
     */

    /**
     * 1 一个点（.）表示当前目录本身；此外，两个点 （..） 表示将目录切换到上一级
     * 2 以斜杠 / 开头
     * 3 两个目录名之间必须只有一个斜杠 /
     * 4 最后一个目录名（如果存在）不能以 / 结尾
     * 5 必须是表示绝对路径的最短字符串
     */
    public static String simplifyPath(String path) {
        Deque<String> deque = new ArrayDeque<>();
        String[] paths = path.split("/");
        for (String s : paths) {
            if ("".equals(s)|| ".".equals(s)) {
                continue;
            }
            if ("..".equals(s)) {
                if (!deque.isEmpty()) {
                    deque.pop();
                }
                continue;
            }
            deque.push(s);
        }
        if (deque.isEmpty()) {
            return "/";
        }
        StringBuilder sb = new StringBuilder();
        while (!deque.isEmpty()) {
            sb.insert(0, deque.pop()).insert(0, "/");

        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println(simplifyPath("/home/") + " =  /home" );
        System.out.println(simplifyPath("/../") + " =  / " );
        System.out.println(simplifyPath("/home//foo/") + " =  /home/foo" );
        System.out.println(simplifyPath("/a/./b/../../c/") + " =  /c" );
        System.out.println(simplifyPath("/a/../../b/../c//.//") + " =  /c" );
        System.out.println(simplifyPath("/a//b////c/d//././/..") + " =  /a/b/c" );
    }
}
