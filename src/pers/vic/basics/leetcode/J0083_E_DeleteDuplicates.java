package pers.vic.basics.leetcode;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @description: 83. 删除排序链表中的重复元素  {@literal https://leetcode-cn.com/problems/remove-duplicates-from-sorted-list/}
 * @author Vic.xu
 * @date: 2021/1/20 0020 8:25
 */
public class J0083_E_DeleteDuplicates {

    /*
    给定一个排序链表，删除所有重复的元素，使得每个元素只出现一次。
     */

    public static ListNode deleteDuplicates(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        Map<Integer, ListNode> map = new LinkedHashMap<>();

        while (head != null) {
            map.put(head.val, head);
            head = head.next;
        }

        ListNode dummy = new ListNode(-1);
        ListNode cur = dummy;
        for (Map.Entry<Integer, ListNode> entry : map.entrySet()) {
            cur.next = entry.getValue();
            cur = entry.getValue();
        }
        return dummy.next;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(new int[]{1,2,3,3,4,4,5});
        System.out.println(head);
        ListNode listNode = deleteDuplicates(head);
        System.out.println(listNode);
    }
}
