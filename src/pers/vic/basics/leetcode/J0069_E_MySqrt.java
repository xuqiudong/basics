package pers.vic.basics.leetcode;

/**
 * @description: 69. x 的平方根 {@literal https://leetcode-cn.com/problems/sqrtx/}
 * @author Vic.xu
 * @date: 2020/12/31 0031 9:48
 */
public class J0069_E_MySqrt {
    /*
    实现 int sqrt(int x) 函数。
    计算并返回 x 的平方根，其中 x 是非负整数。
    由于返回类型是整数，结果只保留整数的部分，小数部分将被舍去。

     */

    /**
     * 数学方法却是不会的，只能二分法笨方法搞起来
     * x 的平方根 其中 x 是非负整数。
     */
    public static int mySqrt(int x) {
        if (x <= 1) {
            return x;
        }
        // 真的不大会二分法的边界划分
        int res = 0;
        int left = 0;
        int right = x;
        while (left <= right) {
            int mid = left + (right - left) / 2;

            long sqrt = (long)mid * (long)mid;
            if (sqrt == x) {
                return mid;
            }
            if (sqrt < x) {
                res = mid;
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return res;
    }

    public static void main(String[] args) {
        System.out.println(mySqrt(2147395599));
        System.out.println(mySqrt(9));
    }

}
