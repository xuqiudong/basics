package pers.vic.basics.leetcode;

import com.sun.xml.internal.bind.v2.model.core.ID;

/**
 * @description: 62. 不同路径 {@literal https://leetcode-cn.com/problems/unique-paths/}
 * @author Vic.xu
 * @date: 2020/12/9 0009 15:08
 */
public class J0062_M_UniquePaths {

    /*
    一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为 “Start” ）。
    机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。
    问总共有多少条不同的路径？
     */

    /**
     *  这是一个典型的简单的动态规划问题
     *  1. 状态记录[m,n] 记录到m,n的唯一路径个数
     *  2. 初始状态，第一行 第一列 步数只能是1 
     *  3. 状态转义：[m,n] = [m-1,n] + [m,n-1]
     */
    public static int uniquePaths(int m, int n) {
        // [m,n] 记录到m,n的唯一路径个数
        int[][] dp = new int[m][n];
        //状态转移   以及初始化 第一行和第一列的唯一路径 数量
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                //初始化 第一列 和第一行
                if (i==0 || j==0) {
                    dp[i][j] = 1;
                    continue;
                }
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }
        return dp[m-1][n-1];
    }

    public static void main(String[] args) {
        //28
        System.out.println(uniquePaths(3, 7));
        //6
        System.out.println(uniquePaths(3, 2));
        //28
        System.out.println(uniquePaths(1, 1));
        //6
       System.out.println(uniquePaths(3, 3));
    }

}
