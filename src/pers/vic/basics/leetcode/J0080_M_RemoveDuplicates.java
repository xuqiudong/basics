package pers.vic.basics.leetcode;

import org.omg.Messaging.SyncScopeHelper;

import java.util.Arrays;

/**
 * @description: 80. 删除排序数组中的重复项 II {@literal https://leetcode-cn.com/problems/remove-duplicates-from-sorted-array-ii/}
 * @see J0026_E_RemoveDuplicates
 * @author Vic.xu
 * @date: 2021/1/18 0018 8:19
 */
public class J0080_M_RemoveDuplicates {

    /*
    给定一个增序排列数组 nums ，你需要在 原地 删除重复出现的元素，使得每个元素最多出现两次，返回移除后数组的新长度。
    不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
     */
    /**
     * 1  1  1  2 2 2 3 3 3
     * 1  1 2
     */
    public static  int removeDuplicates(int[] nums) {
        int len = nums.length;
        if (nums.length <= 2) {
            return len;
        }
        //当前不重复的下标；
        int j = 2;
        for (int i = 2; i < len; i++) {
            if (nums[i] != nums[j-2]) {
                nums[j++] = nums[i];
            }

        }
        return j;
    }

    public static void main(String[] args) {
        int[] nums = {1,1,1,2,2,3};
        System.out.println(removeDuplicates(nums));
        System.out.println(Arrays.toString(nums));
    }
}
