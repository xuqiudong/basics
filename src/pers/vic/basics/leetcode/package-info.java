/**
 * 记录力扣 的一些题目的一些编码 https://leetcode-cn.com/
 *
 *  class命名规则如下：'J' + number + '_' + level +  '_' + describe
 *  1. 以字母J开头
 *  2. numbern 表示题目的序号，四位数
 *  3. leverl表示题目的难度级别：E：easy；M:middle; H:hard
 *  4. describe 题目的大概描述，详见class注释
 */

package pers.vic.basics.leetcode;