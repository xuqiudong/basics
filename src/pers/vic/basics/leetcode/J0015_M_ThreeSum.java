package pers.vic.basics.leetcode;

import java.sql.SQLOutput;
import java.util.*;

/**
 * @author Vic.xu
 * @description: 15. 三数之和
 * @date: 2020/11/9 10:29
 */
public class J0015_M_ThreeSum {
    /*
    给你一个包含 n 个整数的数组 nums，判断 nums 中是否存在三个元素 a，b，c ，使得 a + b + c = 0 ？请你找出所有满足条件且不重复的三元组。
    注意：答案中不可以包含重复的三元组。
     */

    /**
     * 排序 + 双指针
     * 1. 先把数组排序 从小到大
     * 2. 从左到右遍历数组：此时取上指针：当前的右侧 和 数组的尾巴，
     * 3. 两个指针向中心位置移动
     * @param nums
     * @return
     */
    public static List<List<Integer>> threeSum(int[] nums) {
        Set<List<Integer>> set = new HashSet<>();
        Arrays.sort(nums);
        int len = nums.length;

        for (int i = 0; i < len; i++) {
            int l = i + 1;
            int r = len - 1;
            while (l < r) {
                int sum = nums[i] + nums[l] + nums[r];
                if (sum == 0) {
                    set.add(Arrays.asList(nums[i], nums[l], nums[r]));
                    l++;
                    r--;
                } else if (sum < 0) {
                    l++;
                } else {
                    r--;
                }
            }
        }
        return new ArrayList<>(set);
    }

    /*
    给定数组 nums = [-1, 0, 1, 2, -1, -4]，

    满足要求的三元组集合为：
    [
      [-1, 0, 1],
      [-1, -1, 2]
    ]

     */
    public static void main(String[] args) {
        int[] nums = {-1, 0, 1, 2, -1, -4};
        List<List<Integer>> res = threeSum(nums);
        res.forEach(System.out::println);
    }
}
