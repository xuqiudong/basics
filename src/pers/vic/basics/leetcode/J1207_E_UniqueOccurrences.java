package pers.vic.basics.leetcode;

import java.util.*;

/**
 * @description:1207. 独一无二的出现次数
 * 给你一个整数数组 arr，请你帮忙统计数组中每个数的出现次数。
 * 如果每个数的出现次数都是独一无二的，就返回 true；否则返回 false。
 * @author Vic.xu
 * @date: 2020/10/28 13:48
 */
public class J1207_E_UniqueOccurrences {

    /**
     * 每个数出现的次数是唯一的则返回true
     * @param arr
     * @return
     */
    public boolean uniqueOccurrences(int[] arr) {
        //每个数字的出现次数
        Map<Integer, Integer> map = new HashMap<>();
        for (int item : arr) {
            map.put(item, map.getOrDefault(item, 0)+1);
        }
        //不同的次数的集合
        Set<Integer> countSet = new HashSet<>(new ArrayList<>(map.values()));
        return map.size() == countSet.size();
    }
}
