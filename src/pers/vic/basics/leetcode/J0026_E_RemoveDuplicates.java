package pers.vic.basics.leetcode;

import java.util.Arrays;

/**
 * @author Vic.xu
 * @description:26. 删除排序数组中的重复项
 * @date: 2020/11/16  9:13
 */
public class J0026_E_RemoveDuplicates {
    /*
    给定一个排序数组，你需要在 原地 删除重复出现的元素，使得每个元素只出现一次，返回移除后数组的新长度。
    不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。

    给定数组 nums = [1,1,2],
    函数应该返回新的长度 2, 并且原数组 nums 的前两个元素被修改为 1, 2。

    你不需要考虑数组中超出新长度后面的元素。
     */

    /**
     * 是有序的数组
     */
    public static int removeDuplicates(int[] nums) {
        //j 表示不重复的数组的下标
        int j = 0;
        for (int i = 0; i < nums.length; i++) {
            //遇到不相等的  则把指针后移，然后填充上不相等的这个数字
            if (nums[i] != nums[j]) {
                nums[++j] = nums[i];

            }
        }
        //因为当前j 是下标，长度应+1
        return j + 1;
    }


    public static void main(String[] args) {
        int[] nums = {0, 0, 1, 1, 1, 2, 2, 3, 3, 4};
        int i = removeDuplicates(nums);
        //函数应该返回新的长度 5
        System.out.println(i);
        //前五为 0, 1, 2, 3, 4。
        System.out.println(Arrays.toString(nums));
    }
}
