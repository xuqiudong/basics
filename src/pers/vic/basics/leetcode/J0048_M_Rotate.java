package pers.vic.basics.leetcode;

import java.util.Arrays;

/**
 * @description: 48. 旋转图像  {@literal https://leetcode-cn.com/problems/rotate-image/}
 * @author Vic.xu
 * @date: 2020/12/10 0010 14:34
 */
public class J0048_M_Rotate {
    /*
      90度原地旋转n*n 矩阵
     */

    /**
     * 脑瓜疼， 不知道怎么写：
     * 一圈一圈 旋转吧
     *   1   2   3   4
     *   5   6   7   8
     *   9   10  11  12
     *   13  14  15  16
     *  首先第一圈 四个角旋转，然后四个角后面的点旋转 （每次四个点依次交换位置）
     *  [0,0]->[0,3]->[3,3]->[3,0]
     *  [0,1]->[1,3]->[3,2]->[2,0]
     *  [0,2]->[2,3]->[3,1]->[1,0]
     *  然后第二圈：
     *  [1,1]->[1,2]->[2,2]->[2,1]
     *
     */
    public static void rotate(int[][] matrix) {
        int n = matrix.length;
        //总共需要旋转多少圈
        int r = n/2;
        //最外圈需要移动多少步 (每次缩小一圈 则移动步数-2)
        int step = n-1;
        //圈数
        for (int i = 0; i < r; i++) {
            //每圈需要移动多少步
            for (int j = 0; j < step; j++) {
                /*
                四个点分别如下，第一个点赋值给第二个 第二个点赋值给第三个  第三个点赋值给第四个 第四个点赋值给第一个
                 */
                int p1 = matrix[i][i+j];
                int p2 = matrix[i+j][step+i];
                int p3 = matrix[step+i][step-j+i];
                int p4 = matrix[step-j+i][i];
                //第三个赋值给第一个
                matrix[i][i+j] = p4;
                matrix[i+j][step+i] = p1;
                matrix[step+i][step-j+i] = p2;
                matrix[step-j+i][i] = p3;
            }
            step-=2;
        }
    }

    public static void main(String[] args) {
        int[][] matrix = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };
        rotate(matrix);
        for (int[] nums : matrix) {
            System.out.println(Arrays.toString(nums));
        }

        int[][] matrix2 = {
                {1, 2, 3, 4},
                {5, 6, 7, 8},
                {9, 10, 11, 12},
                {13, 14, 15, 16}
        };
        rotate(matrix2);
        for (int[] nums : matrix2) {
            System.out.println(Arrays.toString(nums));
        }
    }

}
