package pers.vic.basics.leetcode;

/**
 * @description: 67. 二进制求和  {@literal https://leetcode-cn.com/problems/add-binary/}
 * @author Vic.xu
 * @date: 2020/12/29 0029 15:33
 */
public class J0067_E_AddBinary {

    /*
    给你两个二进制字符串，返回它们的和（用二进制表示）。
    输入为 非空 字符串且只包含数字 1 和 0。
     */
    public static String addBinary(String a, String b) {
        StringBuilder sb = new StringBuilder();
        int carry = 0;
        for (int i = a.length() - 1, j = b.length() - 1; i >= 0 || j >= 0; i--, j--) {
            carry += i >= 0 ? a.charAt(i) - '0' : 0;
            carry += j >= 0 ? b.charAt(j) - '0' : 0;
            sb.append(carry % 2);
            carry /= 2;

        }
        sb.append(carry == 1 ? 1 : "");
        return sb.reverse().toString();
    }

    public static void main(String[] args) {
        System.out.println(addBinary("10001", "11"));

    }
}
