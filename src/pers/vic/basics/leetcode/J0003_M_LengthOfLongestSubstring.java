package pers.vic.basics.leetcode;

import java.util.HashSet;
import java.util.Set;

/**
 * @description:3.无重复字符的最长子串
 * 给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。
 * @author Vic.xu
 * @date: 2020/10/29 0029 9:16
 */
public class J0003_M_LengthOfLongestSubstring {

    public static int lengthOfLongestSubstring(String s) {
        if (s == null || "".equals(s)) {
            return 0;
        }
        int max = 1;
        String[] arr = s.split("");
        int len = arr.length;
        Set<String> set = new HashSet<>();
        for (int i = 0; i < len; i++) {
            for (int j=i; j<len; j++) {
                if (set.contains(arr[j])) {
                    max = max > set.size() ? max : set.size();
                    set = new HashSet<>();
                    break;
                }else {
                    set.add(arr[j]);
                }

            }
        }

        return max;
    }

    public static void main(String[] args) {
        String s = "";
        int i = lengthOfLongestSubstring(s);
        System.out.println(i);
    }
}
