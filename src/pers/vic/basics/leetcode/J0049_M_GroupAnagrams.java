package pers.vic.basics.leetcode;

import java.util.*;

/**
 * @description: 49. 字母异位词分组  {@literal https://leetcode-cn.com/problems/group-anagrams/}
 * @author Vic.xu
 * @date: 2020/12/11 0011 8:18
 */
public class J0049_M_GroupAnagrams {
    /*
    给定一个字符串数组，将字母异位词组合在一起。字母异位词指字母相同，但排列不同的字符串。
    说明：
    所有输入均为小写字母。
    不考虑答案输出的顺序。
     */

    /**
     * 这个问题还是很简单的：
     * 1. 字符串按照顺序排序， 则相同组成的字符串能转化程相等的新字符串key
     * 2. 然后相等的key对应的字符串放在map对应的list集合中即可
     */
    public static List<List<String>> groupAnagrams(String[] strs) {
        Map<String, List<String>> map = new HashMap<>();
        for (String str : strs) {
            char[] cs = str.toCharArray();
            Arrays.sort(cs);
            String key = new String(cs);
            map.compute(key, (k, v) -> {
                if (v == null) {
                    v = new ArrayList<>();
                }
                v.add(str);
                return v;

            });
        }
        return new ArrayList<>(map.values());
    }

    public static void main(String[] args) {
        String[] strs = {"eat", "tea", "tan", "ate", "nat", "bat"};
        groupAnagrams(strs).forEach(System.out::println);
    }
}
