package pers.vic.basics.leetcode;

/**
 * @description: 70. 爬楼梯  {@literal https://leetcode-cn.com/problems/climbing-stairs/}
 * @author Vic.xu
 * @date: 2021/1/4 0004 8:39
 */
public class J0070_E_ClimbStairs {
    /*
    假设你正在爬楼梯。需要 n 阶你才能到达楼顶。
    每次你可以爬 1 或 2 个台阶。你有多少种不同的方法可以爬到楼顶呢？
    注意：给定 n 是一个正整数。
    */

    /**
     *  这是一道让动态规划很容易解决的问题
     */
    public static int climbStairs(int n) {
        if (n <= 2) {
            return n;
        }
        int[] dp = new int[n+1];
        dp[1] = 1;
        dp[2] = 2;
        for (int i = 3; i <= n; i++) {
            dp[i] = dp[i-1] + dp[i-2];
        }
        return dp[n];
    }

    public static void main(String[] args) {
        System.out.println(climbStairs(22));
    }

}
