package pers.vic.basics.leetcode;

/**
 * @author Vic.xu
 * @description: 12. 整数转罗马数字
 * @date: 2020/11/6  9:09
 */
public class J0012_M_IntToRoman {
    /*
    罗马数字包含以下七种字符： I， V， X， L，C，D 和 M。
    字符          数值
    I             1
    V             5
    X             10
    L             50
    C             100
    D             500
    M             1000
    例如， 罗马数字 2 写做 II ，即为两个并列的 1。12 写做 XII ，即为 X + II 。 27 写做  XXVII, 即为 XX + V + II 。

    通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做 IIII，而是 IV。数字 1 在数字 5 的左边，所表示的数等于大数 5 减小数 1 得到的数值 4 。同样地，数字 9 表示为 IX。这个特殊的规则只适用于以下六种情况：

    I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。
    X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。 
    C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。
    给定一个整数，将其转为罗马数字。输入确保在 1 到 3999 的范围内。
     */
    public static String intToRoman3(int num) {
        // 1  5  10 50 100 500 1000
        //{"I", "V", "X", "L", "D", "M"}

        int[] values = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        String[] romans = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            while (num >= values[i]) {
                num -= values[i];
                sb.append(romans[i]);
            }
        }
        return sb.toString();
    }

    /**
     * 先写一个 我会的 暴力解决的
     * 确定只有四位以内的
     *
     * @param num
     * @return
     */
    public static String intToRoman(int num) {
        if (num < 1 || num > 3999) {
            return null;
        }
        String[] i = {"", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"};
        String[] ii = {"", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"};
        String[] iii = {"", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"};
        String[] iiii = {"", "M", "MM", "MMM"};

        return iiii[num / 1000] + iii[num % 1000 / 100] + ii[num % 100 / 10] + i[num % 10];

    }


    public static void main(String[] args) {
        //III
        System.out.println(intToRoman3(3));
        //IV
        System.out.println(intToRoman3(4));
        //IX
        System.out.println(intToRoman3(9));
        //LVIII
        System.out.println(intToRoman3(58));
        //MCMXCIV
        System.out.println(intToRoman3(1994));
    }


}
