package pers.vic.basics.leetcode;

import java.util.Arrays;

/**
 * @description: 66. 加一  {@literal https://leetcode-cn.com/problems/plus-one/}
 * @author Vic.xu
 * @date: 2020/12/29 0029 9:16
 */
public class J0066_E_PlusOne {

    /*
    给定一个由 整数 组成的 非空 数组所表示的非负整数，在该数的基础上加一。
    最高位数字存放在数组的首位， 数组中每个元素只存储单个数字。
    你可以假设除了整数 0 之外，这个整数不会以零开头。
     */


    public static int[] plusOne(int[] digits) {
        for (int i = digits.length - 1; i >= 0; i--) {
            int temp = digits[i] + 1;
            digits[i] = temp % 10;
            if (temp <10) {
                return  digits;
            }
        }
        //因为只有99 999 类似的才能进位到最后
        int[] res = new int[digits.length + 1];
        res[0] = 1;
        return res;
    }
    public static int[] plusOne2(int[] digits) {
        int carry = 0;
        for (int i = digits.length - 1; i >= 0; i--) {
            int temp = digits[i] + 1;
            if (temp > 9) {
                digits[i] = temp % 10;
                carry = 1;
            } else {
                digits[i] = temp;
                carry = 0;
                break;
            }
        }
        if (carry > 0) {
            int[] res = new int[digits.length + 1];
            res[0] = carry;
            System.arraycopy(digits, 0, res, 1, digits.length);
            return res;

        }
        return digits;
    }

    public static void main(String[] args) {
        int[] digits = {1, 2, 3};
        System.out.println(Arrays.toString(plusOne(digits)));
        int[] digits2 = {9, 9, 9};
        System.out.println(Arrays.toString(plusOne(digits2)));
    }
}
