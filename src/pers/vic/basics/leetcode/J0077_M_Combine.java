package pers.vic.basics.leetcode;

import java.util.*;

/**
 * @description: 77. 组合  {@literal https://leetcode-cn.com/problems/combinations/}
 * @author Vic.xu
 * @date: 2021/1/13 0013 8:50
 */
public class J0077_M_Combine {
    /*
    给定两个整数 n 和 k，返回 1 ... n 中所有可能的 k 个数的组合。
     */

    /**
     * 典型的回溯算法
     */
    public static List<List<Integer>> combine(int n, int k) {
        List<List<Integer>> result = new ArrayList<>();
        if (k > n) {
            return result;
        }
        dfs(result, new ArrayDeque<>(), 1, n, k);
        return result;
    }

    public static void dfs(List<List<Integer>> result, Deque<Integer> path, int start, int n, int k) {
        if (path.size() == k) {
            result.add(new ArrayList<>(path));
            return;
        }
        for (int i = start; i <= n; i++) {
            path.add(i);
            dfs(result, path, i + 1, n, k);
            path.removeLast();
        }
    }

    public static void main(String[] args) {
        combine(4, 2).forEach(System.out::println);
    }
}
