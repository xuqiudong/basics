package pers.vic.basics.leetcode;

import java.util.Arrays;

/**
 * @description: 73. 矩阵置零   {@literal https://leetcode-cn.com/problems/set-matrix-zeroes/}
 * @author Vic.xu
 * @date: 2021/1/8 0008 10:00
 */
public class J0073_M_SetZeroes {

    /*
    给定一个 m x n 的矩阵，如果一个元素为 0，则将其所在行和列的所有元素都设为 0。请使用原地算法。
    进阶:
    一个直接的解决方案是使用  O(mn) 的额外空间，但这并不是一个好的解决方案。
    一个简单的改进方案是使用 O(m + n) 的额外空间，但这仍然不是最好的解决方案。
    你能想出一个常数空间的解决方案吗？
     */
    public static void setZeroes(int[][] matrix) {
        int m = matrix.length;
        int n = matrix[0].length;
        boolean[] rowFlag = new boolean[m];
        boolean[] colmnFlag = new boolean[n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (matrix[i][j] == 0) {
                    rowFlag[i] = true;
                    colmnFlag[j] = true;
                }
            }
        }

        for (int i = 0; i < m; i++) {
            if (rowFlag[i]) {
                //这一行所有元素都置为 0
                for (int j = 0; j < n; j++) {
                    matrix[i][j] = 0;
                }
            }
        }

        for (int i = 0; i < n; i++) {
            if (colmnFlag[i]) {
                //这一列所有元素都置为 0
                for (int j = 0; j < m; j++) {
                    matrix[j][i] = 0;
                }
            }
        }
    }

    public static void main(String[] args) {
        int[][] matrix = {{1,1,1},{1,0,1},{1,1,1}};
        test(matrix);
        int[][] matrix2 = {{0,1,2,0},{3,4,5,2},{1,3,1,5}};
        test(matrix2);

    }

    public static void test(int[][] matrix){
        setZeroes(matrix);
        Arrays.stream(matrix).forEach(row->{
            System.out.println("[ " + Arrays.toString(row) + " ]");
        });
    }
}
