package pers.vic.basics.leetcode;

/**
 * @description: 63. 不同路径 II  {@literal https://leetcode-cn.com/problems/unique-paths-ii/}
 * @author Vic.xu
 * @date: 2020/12/24 0024 8:45
 */
public class J0063_M_UniquePathsWithObstacles {

    /*
    一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为“Start” ）。
    机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为“Finish”）。
    现在考虑网格中有障碍物。那么从左上角到右下角将会有多少条不同的路径？
    网格中的障碍物和空位置分别用 1 和 0 来表示。
     */

    /**
     * 这是我初学动态规划时候的类似的体题目
     */
    public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
        if (obstacleGrid.length == 0 || obstacleGrid[0].length == 0 || obstacleGrid[0][0] == 1) {
            return 0;
        }
        int row = obstacleGrid.length;
        int column = obstacleGrid[0].length;
        int[][] dp = new int[row][column];
        //前面已经判断 [0][0]无障碍物
        dp[0][0] = 1;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < column; j++) {
                if (j == 0 && i == 0) {
                    continue;
                }
                if (obstacleGrid[i][j] == 1) {
                    dp[i][j] = 0;
                    continue;
                }
                //第一列
                if (j == 0) {
                    dp[i][0] = dp[i-1][0];
                    continue;
                }
                //第一行
                if (i == 0) {
                    dp[0][j] = dp[0][j-1];
                    continue;
                }
                dp[i][j] = dp[i-1][j] + dp[i][j-1];
            }
        }
        return dp[row-1][column-1];
    }

    public static void main(String[] args) {
        int[][] nn = {{0,0,0},{0,1,0},{0,0,0}};
        System.out.println(uniquePathsWithObstacles(nn));

    }
}
