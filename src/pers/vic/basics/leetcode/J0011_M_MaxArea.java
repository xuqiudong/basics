package pers.vic.basics.leetcode;

/**
 * @description:11. 盛最多水的容器
 *
 * 给你 n 个非负整数 a1，a2，...，an，每个数代表坐标中的一个点 (i, ai) 。
 * 在坐标内画 n 条垂直线，垂直线 i 的两个端点分别为 (i, ai) 和 (i, 0) 。找出其中的两条线，使得它们与 x 轴共同构成的容器可以容纳最多的水。
 *
 * 说明：你不能倾斜容器。
 * @author Vic.xu
 * @date: 2020/11/5  16:00
 */
public class J0011_M_MaxArea {

    public static int maxArea(int[] height) {
        int left = 0; int right = height.length -1;
        int area = 0;
        while (left < right) {
            //高度取决 最小的高度
            int h = height[left] > height[right] ? height[right] : height[left];
            int curArea = h * (right - left);
            area = curArea > area ? curArea : area;
            //把高度更小的位置 往内部移动，因为若移动高度大的  面积肯定变小
            if (height[left] < height[right]) {
                left++;
            }else {
                right--;
            }
        }
        return area;
    }

    public static void main(String[] args) {
        //49
        System.out.println(maxArea(new int[]{1,8,6,2,5,4,8,3,7}));
        //1
        System.out.println(maxArea(new int[]{1,1}));
        //16
        System.out.println(maxArea(new int[]{4,3,2,1,4}));

        //2
        System.out.println(maxArea(new int[]{1,2,1}));
    }

}
