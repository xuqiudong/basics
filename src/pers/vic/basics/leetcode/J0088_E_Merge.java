package pers.vic.basics.leetcode;

import java.util.Arrays;

/**
 * @author Vic.xu
 * @description: 88. 合并两个有序数组  {@literal https://leetcode-cn.com/problems/merge-sorted-array/}
 * @date: 2021/3/5 0005 8:53
 */
public class J0088_E_Merge {
    /*
    给你两个有序整数数组 nums1 和 nums2，请你将 nums2 合并到 nums1 中，使 nums1 成为一个有序数组。
    初始化 nums1 和 nums2 的元素数量分别为 m 和 n 。你可以假设 nums1 的空间大小等于 m + n，这样它就有足够的空间保存来自 nums2 的元素。
     */

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int index = m-- + n-- - 1;
        while (m >= 0 && n >= 0) {
            //找出大的那个数 方在nums1结尾
            nums1[index--]= nums1[m] > nums2[n] ? nums1[m--] : nums2[n--];
        }
        while (n >=0) {
            nums1[index--] = nums2[n--];
        }
    }

    public static void merge2(int[] nums1, int m, int[] nums2, int n) {
        System.arraycopy(nums2, 0, nums1, m, n);
        Arrays.sort(nums1);
    }

    public static void main(String[] args) {
        int[] nums1 = {1, 2, 3, 0, 0, 0};
        int m = 3;
        int[] nums2 = {2, 5, 6};
        int n = 3;
        merge(nums1, m, nums2, n);
        System.out.println(Arrays.toString(nums1));

    }

}
