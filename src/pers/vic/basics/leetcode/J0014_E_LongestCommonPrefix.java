package pers.vic.basics.leetcode;

/**
 * @description:14. 最长公共前缀
 * @author Vic.xu
 * @date: 2020/11/6 0006 17:46
 */
public class J0014_E_LongestCommonPrefix {
    public static String longestCommonPrefix(String[] strs) {

        StringBuilder sb = new StringBuilder();
        //当前获取字符串的下标
        int index = 0;
        outer:while (true) {
            String cur = null;
            for (int i = 0; i < strs.length; i++) {
                if (index>= strs[i].length()) {
                    break outer;
                }
                if (i == 0) {
                    cur = strs[i].substring(index, index+1);
                }else {
                    if (!cur.equals(strs[i].substring(index, index+1))) {
                        break outer;
                    }
                }

            }
            if (cur == null) {
                break outer;
            }
            sb.append(cur);
            index++;

        }
        return sb.toString();

    }

    public static void main(String[] args) {
        //"fl"
        System.out.println(longestCommonPrefix(new String[]{"flower","flow","flight"}));
        //""
        System.out.println(longestCommonPrefix(new String[]{"dog","racecar","car"}));
    }


}
