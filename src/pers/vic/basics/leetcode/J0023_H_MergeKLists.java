package pers.vic.basics.leetcode;

import java.util.List;

/**
 * @author Vic.xu
 * @description: 23. 合并K个升序链表     分治算法还不会TODO， 先暴力解决2020年11月13日
 * @date: 2020/11/13 8:15
 */
public class J0023_H_MergeKLists {
    /*
    给你一个链表数组，每个链表都已经按升序排列。
    请你将所有链表合并到一个升序链表中，返回合并后的链表。
     */

    /**
     * @return 1->1->2->3->4->4->5->6
     */
    public static ListNode mergeKLists(ListNode[] lists) {
        if (lists == null || lists.length == 0) {
            return null;
        }
        ListNode merge = lists[0];

        for (int i = 1; i < lists.length; i++) {
            merge = mergerTwo(merge, lists[i]);
        }
        return merge;
    }

    /**
     * 两两合并
     */
    public static ListNode mergerTwo(ListNode l1, ListNode l2) {
        ListNode dummy = new ListNode();
        ListNode pref = dummy;
        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                pref.next = l1;
                l1 = l1.next;
            } else {
                pref.next = l2;
                l2 = l2.next;
            }
            pref = pref.next;
        }
        if (l1 != null) {
            pref.next = l1;
        } else if (l2 != null) {
            pref.next  = l2;
        }
        return dummy.next;
    }


    public static void main(String[] args) {

        ListNode[] lists = new ListNode[]{ new ListNode(new int[]{1,3}),  new ListNode(new int[]{2,4,8}),  new ListNode(new int[]{5,6,9})};

        ListNode m = mergeKLists(lists);
        System.out.println(m);
    }
}
