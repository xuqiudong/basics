package pers.vic.basics.leetcode;

import java.util.Arrays;

/**
 * @author Vic.xu
 * @description: 27. 移除元素  {@literal https://leetcode-cn.com/problems/remove-element/}
 * @date: 2020/11/18 8:19
 */
public class J0027_E_RemoveElement {
    /*
    给你一个数组 nums 和一个值 val，你需要 原地 移除所有数值等于 val 的元素，并返回移除后数组的新长度。
    不要使用额外的数组空间，你必须仅使用 O(1) 额外空间并 原地 修改输入数组。
    元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。
     */

    /**
     *  简单的题我会....[飙泪]
     */
    public static int removeElement(int[] nums, int val) {
        //实际下标
        int j = 0;
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] != val) {
                nums[j++] = nums[i];
            }
        }
        return j;
    }

    public static void main(String[] args) {
        int[] nums = {0,1,2,2,3,0,4,2};
        int val = 2;
        //新的长度 5, 并且 nums 中的前五个元素为 0, 1, 3, 0, 4。
        System.out.println(removeElement(nums, val));
        System.out.println(Arrays.toString(nums));
    }
}
