package pers.vic.basics.leetcode;

import sun.awt.SunGraphicsCallback;

import java.util.Arrays;

/**
 * @author Vic.xu BinaryNode@literal https://leetcode-cn.com/problems/valid-sudoku/}
 * @description: 36. 有效的数独
 * @date: 2020/11/27 8:32
 */
public class J0036_M_IsValidSudoku {

    /*
    判断一个 9x9 的数独是否有效。只需要根据以下规则，验证已经填入的数字是否有效即可。
    数字 1-9 在每一行只能出现一次。
    数字 1-9 在每一列只能出现一次。
    数字 1-9 在每一个以粗实线分隔的 3x3 宫内只能出现一次。
    数独部分空格内已填入了数字，空白格用 '.' 表示。
     */
    static int L = 9;

    /**
     * 如何判断重复：通过位移算法 int[9] arr   (arr[x]>>x &1) == 1
     *
     * @param board
     * @return
     */
    public static boolean isValidSudoku(char[][] board) {
        // 每个元素存储整行的数字 按位存储  如 先存元素4    --> 000000100， 再存元素1  --> 000000101
        int[] rows = new int[L];
        //每个元素存一整列 如，[0] 存 第一列  [1]存第二列
        int[] columns = new int[L];
        // 每个元素存 一个3X3的九宫格：  [index] = i/3*3 + j/3
        int[] boxes = new int[L];
        for (int i = 0; i < L; i++) {
            for (int j = 0; j < L; j++) {
                char cur = board[i][j];
                int boxIndex = i / 3 * 3 + j / 3;
                // 分别验证所在行、列和九宫格 是否已经包含当前元素
                boolean contain = contain(cur, rows, i)||contain(cur, columns, j) || contain(cur, boxes, boxIndex);
                if (contain) {
                    return false;
                }
            }
        }

        return true;
    }

    // 判断当前1-9数字是否已经被包含，没有的话则放入数组
    private static boolean contain(char c, int[] nums, int index) {

        if ('.' == c || index >= L) {
            return false;
        }
        int n = c - '0';
        // 把当前元素取出来  然后匹配第n位  看是否相等
        boolean contain = (nums[index] >> n & 1) == 1;
        if (!contain) {
            //不存在  则把当前数字 放置到元素的对应位置
            nums[index] = nums[index] | 1 << n;
        }
        return contain;
    }

    public static void main(String[] args) {
        char[][] board3 = {
                {'1', '2', '3', '4', '5', '6', '7', '8', '9'},
                {'4', '5', '6', '1', '9', '5', '.', '.', '.'},
                {'5', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'2', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'3', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'6', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'7', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'8', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'9', '.', '.', '.', '8', '.', '.', '7', '9'}
        };
        System.out.println(isValidSudoku(board3));

        //true
        char[][] board = {
                {'5', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'}
        };
        System.out.println(isValidSudoku(board));

        //false
        char[][] board2 = {
                {'8', '3', '.', '.', '7', '.', '.', '.', '.'},
                {'6', '.', '.', '1', '9', '5', '.', '.', '.'},
                {'.', '9', '8', '.', '.', '.', '.', '6', '.'},
                {'8', '.', '.', '.', '6', '.', '.', '.', '3'},
                {'4', '.', '.', '8', '.', '3', '.', '.', '1'},
                {'7', '.', '.', '.', '2', '.', '.', '.', '6'},
                {'.', '6', '.', '.', '.', '.', '2', '8', '.'},
                {'.', '.', '.', '4', '1', '9', '.', '.', '5'},
                {'.', '.', '.', '.', '8', '.', '.', '7', '9'}
        };
        System.out.println(isValidSudoku(board2));

    }
}
