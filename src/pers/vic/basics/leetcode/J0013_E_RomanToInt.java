package pers.vic.basics.leetcode;

import java.util.Random;

/**
 * @author Vic.xu
 * @description: 13. 罗马数字转整数
 * @date: 2020/11/6  17:08
 */
public class J0013_E_RomanToInt {
    /**
     * 从末尾（最右边）开始累加 ，遇到左边的小于右边的 则总数减去当前值
     */
    public static int romanToInt(String s) {
        int sum = 0;
        char[] cs = s.toCharArray();
        int len = cs.length;
        int right = getValue(cs[len - 1]);
        sum = right;
        for (int i = len - 2; i >= 0; i--) {
            int left = getValue(cs[i]);
            if (left < right) {
                sum = sum - left;
            }else {
                sum += left;
            }
            right = left;
        }
        return sum;

    }

    public static int getValue(char c) {
        switch (c) {
            case 'I':
                return 1;
            case 'V':
                return 5;
            case 'X':
                return 10;
            case 'L':
                return 50;
            case 'C':
                return 100;
            case 'D':
                return 500;
            case 'M':
                return 1000;
            default:
                return 0;
        }
    }

    /*
    I             1
    V             5
    X             10
    L             50
    C             100
    D             500
    M             1000
     */

    public static void main(String[] args) {
        System.out.println(romanToInt("III"));
        System.out.println(romanToInt("IV"));
        // 9
        System.out.println(romanToInt("IX"));
        //        58
        System.out.println(romanToInt("LVIII"));
        //1994
        System.out.println(romanToInt("MCMXCIV"));

    }
}
