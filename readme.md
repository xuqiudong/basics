[toc]

# java基础

## 1 数据结构

 了解常用的[数据结构](https://www.cnblogs.com/ysocean/category/1120217.html)以及他们的实现方式

## 2 一些排序

常见的排序方法，以及它们的效率（空间和实践复杂度）。

比如常见的九大排序：

归并排序
 基数排序
 堆排序
 计数排序
 冒泡排序
 选择排序
 插入排序
 希尔排序
 快速排序



## 3 [力扣刷题简单记录](/docs/leetcode)

 了解一点点基础的算法